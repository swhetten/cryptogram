
#include "prepare_words.h"

void free_all_match_nodes(match_node_t *node) {
    match_node_t *cur = node;
    match_node_t *tmp;
    while(cur != NULL) {
        if(cur->words != NULL) {
            int i;
            for(i = 0; i < cur->numWords; ++i) free(cur->words[i]);
            free(cur->words);
        }
        if(cur->seed != NULL) free(cur->seed);
        tmp = cur->next;
        free(cur);
        cur = tmp;
    }
}

static match_node_t *new_node(const char *seed, const int numWords, char **words) {
    match_node_t *newNode;
    newNode = (match_node_t *)malloc(sizeof(match_node_t));
    newNode->seed = calloc(strlen(seed) + 1, sizeof(char));
    strcpy(newNode->seed, seed);
    to_uppercase(newNode->seed);
    newNode->numWords = numWords;
    newNode->occurances = 1;
    newNode->words = words;
    newNode->last = NULL;
    newNode->next = NULL;
}

static char *read_next_word(const char *in, const int length, int *cursor, char *buffer, const int max) {
    char *bCursor = buffer;
    int numRead = 0; 
    do {
        *bCursor = in[*cursor];
        ++*cursor;
    } while(
        (*bCursor < 'A' || (*bCursor > 'Z' && *bCursor < 'a') || *bCursor > 'z')
        && *bCursor != EOF && *cursor < length
    );
    if(*cursor >= length) return NULL;
    do {
        ++bCursor;
        ++numRead;
        *bCursor = in[*cursor];
        ++*cursor;
    } while(
        (*bCursor >= 'A' && *bCursor <= 'Z' || *bCursor >= 'a' && *bCursor <= 'z')
        && *bCursor != EOF && *cursor < length && numRead <= max
    );
    *bCursor = '\0';
    return buffer;
}

static match_node_t *insert_after(match_node_t *headNode, match_node_t *node, match_node_t *last) {
    if(headNode == node || node == last) return headNode;
    if(last == NULL) {
        node->next = headNode;
        headNode->last = node;
        return node;
    } else {
        node->next = last->next;
        last->next = node;
        node->last = last;
        if(node->next != NULL) {
            ((match_node_t *)node->next)->last = node;
        }
        return headNode;
    }
}

static match_node_t *cut_out_node(match_node_t *node) {
    match_node_t *cur = node->last;
    if(cur != NULL && cur->next != node->next)
        cur->next = node->next;
    cur = node->next;
    if(cur != NULL && cur->last != node->last)
        cur->last = node->last;
    node->next = NULL;
    node->last = NULL;
}

static match_node_t *re_sort_occurances(match_node_t *headNode, match_node_t *node, const int seedLen) {
    if(node == headNode) return headNode;
    match_node_t *cur = node->last;
    cut_out_node(node);
    
    while(cur != NULL && cur->numWords == node->numWords && seedLen == strlen(cur->seed) && cur->occurances < node->occurances) 
        cur = cur->last;
    return insert_after(headNode, node, cur);
}

static match_node_t *insert_sorted(match_node_t *headNode, match_node_t *node) {
    match_node_t *cur = headNode;
    match_node_t *last = NULL;
    while(cur != NULL && cur->numWords < node->numWords) {
        last = cur;
        cur = cur->next;
    }
    int seedLen = strlen(node->seed);
    while(cur != NULL && cur->numWords == node->numWords && seedLen < strlen(cur->seed)) {
        last = cur;
        cur = cur->next;
    }
    while(cur != NULL && cur->numWords == node->numWords && seedLen == strlen(cur->seed)) {
        if(strcmp(cur->seed, node->seed) == 0) {
            cur->occurances = cur->occurances + 1;
            return re_sort_occurances(headNode, cur, seedLen);
        }
        last = cur;
        cur = cur->next;
    }
    return insert_after(headNode, node, last);
}

static match_node_t *get_word_possibilities(FILE *fdict, match_node_t *headNode, const char *seed) {
    unsigned short int numWords = 0;
    char **words = read_dictionary(fdict, seed, &numWords);
    match_node_t *node = new_node(seed, numWords, words);
    if (headNode == NULL) return node;
    else insert_sorted(headNode, node);
}

static void add_knowns(match_node_t *node, char *knowns) {
    int i;
    for(i = 0; i < strlen(node->seed); ++i)
        knowns[node->seed[i]-'A'] = 0;        
}

static float calc_known_factor(match_node_t *node, char *knowns) {
    int numKnown = 0;
    int len = strlen(node->seed);
    int i;
    char done[26] = { 0 };
    for(i = 0; i < len; ++i) {
        if(!done[node->seed[i]-'A']) {
            numKnown = numKnown + knowns[node->seed[i]-'A'];
            done[node->seed[i]-'A'] = 1;
        }
    }
    float k = ((float)numKnown / (float)len) * ((float)node->numWords);
    return k;
}

static void order_by_known_factor(match_node_t *headNode, match_node_t *rest, char *knowns) {
    if(rest == NULL) return;
    float bestKnown = calc_known_factor(rest, knowns);
    match_node_t *best = rest;
    match_node_t *cur = rest->next;
    while(cur) {
        float curKnown = calc_known_factor(cur, knowns);
        if(bestKnown > curKnown) {
            bestKnown = curKnown;
            best = cur;
        }
        cur = cur->next;
    }
    if(best != rest) {
        cut_out_node(best);
        insert_after(headNode, best, rest->last);
    }
    add_knowns(best, knowns);
    order_by_known_factor(headNode, best->next, knowns);
}

static match_node_t *order_words(match_node_t *headNode) {
    char knowns[26] = {
        1,1,1,1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,1,1,
        1,1,1,1,1,1
    };
    add_knowns(headNode, knowns);
    order_by_known_factor(headNode, headNode->next, knowns);
    return headNode;
}

match_node_t *prepare_words(FILE *fdict, const char *input, const int length) {
    if(!input) return NULL;
    char buffer[128];
    int cursor = 0;
    match_node_t *headNode = NULL;
    while (read_next_word(input, length, &cursor, buffer, 128))
        headNode = get_word_possibilities(fdict, headNode, buffer);
    headNode = order_words(headNode);
    return headNode;
}