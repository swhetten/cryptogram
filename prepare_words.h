/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _PREPWORDS_HEADER_
#define _PREPWORDS_HEADER_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dictionary/pattern.h"
#include "dictionary/read_dictionary.h"

typedef struct {
    char *seed;
    int numWords;
	int occurances;
    char **words;
	void *last;
    void *next;
} match_node_t;

void free_all_match_nodes(match_node_t *node);
match_node_t *prepare_words(FILE *fdict, const char *input, const int length);

#endif