/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _CRYPTOGRAM_HEADER_
#define _CRYPTOGRAM_HEADER_

#include <stdio.h>
#include <libgen.h>
#include <string.h>
#include <stdlib.h>

#include "prepare_words.h"
#include "decrypt.h"

#define NO_MORE_ARGS -1

#define CRYPTOGRAM_VERSION_IDENTIFIER "0.0"

#endif
