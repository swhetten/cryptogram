#include "decrypt.h"

static char *add_word_to_key(char *key, const char *seed, const char *word) {
    int i;
    for(i = 0; i < strlen(seed); ++i)
        key[seed[i]-'A'] = word[i];
    return key;
}

static int is_match(const char *key, const char *seed, const char *word) {
    int i;
    int j;
    //printf("is_match s:%s -- w:%s\n", seed, word);
    for(i = 0; i < strlen(seed); ++i) {
        //printf("   sk:%i wk:%i sd:%c w:%c\n", key[seed[i]-'A'], key[word[i]-'A'], key[seed[i]-'A'], word[i]);
        if(key[seed[i]-'A'] != 0 && key[seed[i]-'A'] != word[i]) return 0;
        else if (key[seed[i]-'A'] == 0)
            for(j = 0; j < 26; ++j)
                if(key[j] == word[i]) return 0;
    }
    return 1;
}

static char *get_next_matching_word(const char *key, const match_node_t *node, int *matchCursor) {
    for(*matchCursor; *matchCursor < node->numWords; ++*matchCursor)
        if(is_match(key, node->seed, node->words[*matchCursor])) {
            //printf("was match %s=%s\n", node->seed, node->words[*matchCursor]);
            ++*matchCursor;
            return node->words[*matchCursor-1];
        }
    return NULL;
}

static int decrypt_inner(char *key, const match_node_t *node) {
    if(node == NULL) return 1;
    char myKey[26];
    int isDecrypted = 0;
    int matchCursor = 0;
    char *wordToTry = get_next_matching_word(key, node, &matchCursor);
    while (!isDecrypted && wordToTry != NULL) {
        //printf("trying %s\n", wordToTry);
        memcpy(myKey, key, 26 * sizeof(char));
        add_word_to_key(myKey, node->seed, wordToTry);
        isDecrypted = decrypt_inner(myKey, node->next);
        if(isDecrypted)
            memcpy(key, myKey, 26 * sizeof(char));
        else
            wordToTry = get_next_matching_word(key, node, &matchCursor);
    }
    return isDecrypted;
}

char *convert_to_offsets(char *key) {
    int i;
    for(i = 0; i < 26; ++i)
        if(key[i] != 0)
            key[i] = key[i] - ALPHABET[i];
}

char *decrypt(char *input, const match_node_t *headNode) {
    char key[26] = { 0 };
    decrypt_inner(key, headNode);
    int i;
    printf("KEY: %s\n     ", ALPHABET);
    for(i = 0; i < 26; i++)
        if(key[i] == 0) printf("0");
        else printf("%c", key[i]);
    printf("\n");
    convert_to_offsets(key);
    for(i = 0; i < strlen(input); ++i) {
        char charKey = 0;
        if('A' <= input[i] && input[i] <= 'Z')
            charKey = key[input[i]-'A'];
        else if ('a' <= input[i] && input[i] <= 'z')
            charKey = key[input[i]-'a'];
        input[i] = input[i] + charKey;
    }
    return input;
}