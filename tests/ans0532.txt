The gallant coachman of a decade ago has given way to the chauffeur of
to-day.  But we find that his livery is no less important. It is governed
by a very definite convention.  In winter, for instance, the chauffeur
wears long trousers of melton or kersey or similar material and a
double-breasted greatcoat of the same material.  The collar and cuffs may
be of a contrasting color or of the same color as the rest of the
material.  He wears a flat cap with a stiff visor and a band of the same
contrasting color that appears on the collar and cuffs of the coat.  Dark
gloves and shoes are worn.  Sometimes, instead of long trousers, the
chauffeur wears knee-trousers with leather leggings.  It desired, a
double row of brass, silver or polished horn buttons may decorate the
front of the greatcoat, but this must be determined by the prevailing
custom.  If the weather is extremely cold, the chauffeur should be
provided with a long coat of goat or wolf-skin, or some other suitable
protection against the cold and wind.  During the summer months, the
chauffeur usually wears gray or brown cords, developed in the
conventional style.  His cap and gloves match.
