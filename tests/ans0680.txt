"Are you afraid of a lion? I am not. There is nothing that I
should like better than to meet one," said a man to his neighbor
whose calf the lion had killed. "To-morrow morning I will go out
and hunt for this fierce lion, which is doing so much harm. If he
is anywhere about, I shall find him and kill him, and thus rid the
village of fear."
