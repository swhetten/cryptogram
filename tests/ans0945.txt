And so thought Nicholas, whose only trouble
  (At least his worst) was this, his rib's propensity;
For sometimes from the ale-house he would hobble,
  His senses lost in a sublime immensity
Of cogitation--then he couldn't cobble--
  And then his wife would often try the density
Of his poor skull, and strike with all her might,
As fast as kitchen wenches strike a light.
