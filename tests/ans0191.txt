A president of a democracy is a man who is always ready,
willing, and able to lay down your life for his country.
