The appointments of the modern dinner table are a delight. Services are
of silver and china is of the finest.  Both the square or round table are
appropriate, the latter being the most popular since it is easier to make
attractive.  A mat of asbestos or a thickness of canton flannel is first
spread on the table.  Over this comes the snowy, linen table-cover,
falling gracefully over the sides with the four points almost touching
the floor.  A place is laid for each guest.  The most fashionable method
is to have a large lace or embroidered doily in the center of the table,
and smaller ones indicating the position of the guests.  A centerpiece of
glass, china, silver, is usually used, over the doily or without it, and
on top of this, flowers. Delicate ferns are sometimes used instead of
flowers, although roses (hot-house roses when no others are obtainable)
are always the favorite at an elaborate dinner.
