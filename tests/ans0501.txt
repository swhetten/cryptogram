A great many people feel somehow that those who labor in the capacity of
servants are inferior.  But in most cases, it is those who place servants
on a lower plane who are themselves inferior.  We owe those who take a
part in the household affairs of our homes, more than the wages we pay
them.  We owe them gratitude, courtesy, kindness.  Many elaborate dinners
would be failures if it were not for the silent members of our
households. Many formal entertainments would be impossible without their
help. They hold a certain place of importance in the home and it should
be recognized in the social world as a place worthy of every courtesy and
respect.
