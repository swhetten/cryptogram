They threw down their sickles and ran to the flock. But they found
the sheep quietly grazing, and there was no wolf to be seen.
