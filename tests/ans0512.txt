It hardly seems necessary to mention that the servant must be
scrupulously honest.  Perhaps, in their capacity in the home, they are
exposed to unusual temptations, but that is just the reason why they
should refrain from dishonesty of any kind, even the slightest lie.
Gossip about the family life of the people they are serving should also
be avoided by servants.
