The vision was no more--and Nick alone--
  "His streamer's waving" in the midnight wind,
Which through the ruins ceased not to groan;
  --His garment, too, was somewhat short behind,--
And, worst of all, he knew not where to find
  The ring,--which made him most his fate bemoan--
The iron ring,--no doubt of some trap door,
'Neath which the old dead Miser kept his store.
