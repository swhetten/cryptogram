Mr. Graypate walked out to the front and took charge of the
meeting. It was well that they chose him, for he was the wisest
mouse in the whole country. Gazing over the crowd, he said, "Will
Mr. Longtail tell us why we have met here? Mr. Longtail, come out
in front where we can hear you."
