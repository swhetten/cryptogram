The sons would not listen to their father. Each wanted the best of
everything. Each thought the father did more for the others than
for him.
