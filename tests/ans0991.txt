I'm going by the Rail, my dears, where the engines puff and hiss;
And ten to one the chances are that something goes amiss;
And in an instant, quick as thought--before you could cry "Ah!"
An accident occurs, and--say good-by to poor Papa!
