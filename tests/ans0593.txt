Important indeed are the duties of the hostess, for it is upon her that
the ultimate success of the dinner depends.  It is not enough to send out
the invitations, plan a delectable menu and supervise the laying of the
table.  She must afford pleasant diversion and entertainment for her
guests from the minute they enter her home until they are ready to leave.
The ideal hostess is the one who can make her guests, one and all, feel
better satisfied with themselves and the world in general when they leave
her home than they did when they arrived.
