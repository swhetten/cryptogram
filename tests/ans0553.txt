The greatest pride of the American hostess is her formal dinner. And it
is to her credit that we mention that she can hold her own against the
most aristocratic families of Europe.
