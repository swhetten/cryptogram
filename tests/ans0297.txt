So, resolutely turning her back upon the house, she set out once
more down the path, determined to keep straight on till she got to
the hill.  For a few minutes all went on well, and she was just
saying, 'I really SHALL do it this time--' when the path gave a
sudden twist and shook itself (as she described it afterwards),
and the next moment she found herself actually walking in at the
door.
