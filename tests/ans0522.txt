In the sixteenth century French women servants were arrested and placed
in prison for wearing clothes similar to those worn by their "superiors"
It developed that they had made the garments themselves, copying them
from the original models, sometimes sitting up all night to finish the
garment.  But the court ruled that it made no difference whether they had
made them themselves or not; they had worn clothes like their
mistresses', and they must be punished! We very much wiser people of the
twentieth century smile when we read of these ridiculous edicts of a
long-ago court, but we placidly continue to condemn the shop-girl and the
working-girl if she dares to imitate Parisienne importations.
