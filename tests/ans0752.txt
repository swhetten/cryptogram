Once there was a little pink Rosebud,
and she lived down in a little dark house
under the ground.  One day she was sitting
there, all by herself, and it was very
still.  Suddenly, she heard a little TAP, TAP,
TAP, at the door.
