Information travels more surely to those with a
lesser need to know.
