The two little mice nibbled and nibbled,
and the Country Mouse thought he
had never tasted anything so delicious in
his life.  He was just thinking how lucky
the City Mouse was, when suddenly the
door opened with a bang, and in came the
cook to get some flour.
