Upon their page, transfixed with numerous darts,
  Let slender youths in agony expire;
Or, on one spit, let two pale pink calves' hearts
  Roast at some fierce imaginary fire.
