  'Here are the Red King and the Red Queen,' Alice said (in a
whisper, for fear of frightening them), 'and there are the White
King and the White Queen sitting on the edge of the shovel--and
here are two castles walking arm in arm--I don't think they can
hear me,' she went on, as she put her head closer down, 'and I'm
nearly sure they can't see me.  I feel somehow as if I were
invisible--'
