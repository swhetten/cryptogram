One learned gentleman, "a sage grave man,"
  Talk'd of the Ghost in Hamlet, "sheath'd in steel:"--
His well-read friend, who next to speak began,
  Said, "That was Poetry, and nothing real;"
A third, of more extensive learning, ran
  To Sir George Villiers' Ghost, and Mrs. Veal;
Of sheeted Specters spoke with shorten'd breath,
And thrice he quoted "Drelincourt on Death."
