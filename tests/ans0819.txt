Suddenly he saw a big yellow light at
the very edge of the sky.  He thought it
was the Moon.  "Look out, I am coming!"
he cried, and steered for the light.
