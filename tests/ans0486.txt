  'Oh, things that happened the week after next,' the Queen
replied in a careless tone.  'For instance, now,' she went on,
sticking a large piece of plaster [band-aid] on her finger as she
spoke, 'there's the King's Messenger.  He's in prison now, being
punished:  and the trial doesn't even begin till next Wednesday:
and of course the crime comes last of all.'
