No better divan need the Sultan require,
Than the creaking old sofa that basks by the fire;
And 'tis wonderful, surely, what music you get
From the rickety, ramshackle, wheezy spinet.
