After a while he came to Madrid and
to the palace of the King.  Hoppity-kick,
hoppity-kick, the little Half-Chick skipped
past the sentry at the gate, and hoppity-
kick, hoppity-kick, he crossed the court.
But as he was passing the windows of the
kitchen the Cook looked out and saw him.
