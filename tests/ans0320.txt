  'I think I'll go and meet her,' said Alice, for, though the
flowers were interesting enough, she felt that it would be far
grander to have a talk with a real Queen.
