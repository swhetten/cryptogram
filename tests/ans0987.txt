"Surely," I said, as in my chest I dived--
  That vast receptacle of all things known--
"To teach this truth my outfit was contrived,
  It is not good for man to be alone!"
Then fly with me! My bark is on the shore
  (Her mark A 1, her size eight hundred tons),
And though she's nearly full, can take some more
  Dry goods, by measurement--say GREEN and SONS.
