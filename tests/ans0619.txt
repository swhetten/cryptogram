Of course, it is not always agreeable to the hostess to call on one of
her friends to attend her dinner in the place of someone else; but it is
certainly a better plan than to leave the guest out entirely, and have
one more lady than gentleman, or vice versa.  If the note is cordial and
frankly sincere, a good friend will not feel any unreasonable resentment,
but will, in fact, be pleased to serve.
