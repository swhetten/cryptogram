There he stands to this day, with his one
eye, his one wing, and his one leg.  He
cannot hoppity-kick any more, but he turns
slowly round when the wind blows, and
keeps his head toward it, to hear what it
says.
