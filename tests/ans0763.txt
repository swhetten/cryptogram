On earth it grew hotter and hotter; the
sun burned down so fiercely that the people
were fainting in its rays; it seemed as if
they must die of heat, and yet they were
obliged to go on with their work, for they
were very poor.  Sometimes they stood and
looked up at the Cloud, as if they were
praying, and saying, "Ah, if you could
help us!"
