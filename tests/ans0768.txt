Soon that, too, was gone, but long, long
afterward the men and animals who were
saved by the Cloud kept her blessing in
their hearts.
