  'It sounds like a horse,' Alice thought to herself.  And an
extremely small voice, close to her ear, said, 'You might make a
joke on that--something about "horse" and "hoarse," you know.'
