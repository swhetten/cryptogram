One day wolves did come. John was very much frightened. He ran to
the men for help. They only laughed at him. "Oh, you have fooled
us twice," they said. "You shall not have another chance."
