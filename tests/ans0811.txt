So the good Moon made a light and
went along with him, and the little trundle-
bed boat went sailing down the streets
into the main street of the village.  They
rolled past the town hall and the schoolhouse
and the church; but nobody saw
little Jack Rollaround, because everybody
was in bed, asleep.
