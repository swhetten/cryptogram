If the hostess finds, almost at the last moment, that one of her guests
is unavoidably detained and will not be able to attend the dinner, she
may call upon a friend to take the vacant place.  The friend thus invited
should not feel that he or she is playing "second-fiddle" and the fact
that she was not invited at first should not tempt her to refuse the
invitation which would be a serious discourtesy, indeed.  Quite on the
contrary, she should accept cordially, and then do her utmost to make her
(or his, as the case may be) presence at the dinner amiable and pleasant.
