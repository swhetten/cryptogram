  'Now then!  Show your ticket, child!' the Guard went on,
looking angrily at Alice.  And a great many voices all said
together ('like the chorus of a song,' thought Alice), 'Don't
keep him waiting, child!  Why, his time is worth a thousand
pounds a minute!'
