  'This must be the wood, she said thoughtfully to herself,
'where things have no names.  I wonder what'll become of MY name
when I go in?  I shouldn't like to lose it at all--because
they'd have to give me another, and it would be almost certain to
be an ugly one.  But then the fun would be trying to find the
creature that had got my old name!  That's just like the
advertisements, you know, when people lose dogs--"ANSWERS TO
THE NAME OF 'DASH:' HAD ON A BRASS COLLAR"--just fancy calling
everything you met "Alice," till one of them answered!  Only they
wouldn't answer at all, if they were wise.'
