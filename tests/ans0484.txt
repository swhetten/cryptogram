  'You couldn't have it if you DID want it,' the Queen said.
'The rule is, jam to-morrow and jam yesterday--but never jam
to-day.'
