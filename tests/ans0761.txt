"Oh, if I could only help the poor
people down there!" she thought.  "If I could
but make their work easier, or give the
hungry ones food, or the thirsty a drink!"
