For the convenience of the host, it is a point of courtesy for every
recipient of an invitation to dinner, to answer promptly. A good rule is
to decide immediately upon receiving it whether or not you will be able
to attend, and follow it with a cordial answer within the next
twenty-four hours.  If you find that you must refuse, there must be a
very good reason for doing so.
