  It was not a very difficult question to answer, as there was
only one road through the wood, and the two finger-posts both
pointed along it.  'I'll settle it,' Alice said to herself, 'when
the road divides and they point different ways.'
