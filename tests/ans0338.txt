  Alice never could quite make out, in thinking it over
afterwards, how it was that they began:  all she remembers is,
that they were running hand in hand, and the Queen went so fast
that it was all she could do to keep up with her:  and still the
Queen kept crying 'Faster! Faster!' but Alice felt she COULD NOT
go faster, though she had not breath left to say so.
