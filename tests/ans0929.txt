Come out, love--the night is enchanting!
    The moon hangs just over Broadway;
The stars are all lighted and panting--
    (Hot weather up there, I dare say!)
'Tis seldom that "coolness" entices,
    And love is no better for chilling--
But come up to Thompson's for ices,
    And cool your warm heart for a shilling!
