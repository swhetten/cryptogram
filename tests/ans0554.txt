There is a story told of a well-known New York society matron who gave a
formal dinner party on every occasion that warranted it, no matter how
trivial, for the reason that it gave her keen pleasure and enjoyment to
do so.  At one of her dinners recently a famous world-touring lecturer
was the guest of honor--and the hostess was as happy and proud as it is
possible for a hostess to be.  Especially was she proud of the delectable
menu she had ordered prepared for the occasion.
