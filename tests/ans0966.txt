And down the winding stair, with noiseless tread,
  The tenant of the tomb pass'd slowly on,
Each mazy turning of the humble shed
  Seem'd to his step at once familiar grown,
So safe and sure the labyrinth did he tread
  As though the domicile had been his own,
Though Nick himself, in passing through the shop,
Had almost broke his nose against the mop.
