A fair Cathedral, too, the story goes,
  And kings and heroes lie entombed within her;
There pious Saints, in marble pomp repose,
  Whose shrines are worn by knees of many a Sinner;
There, too, full many an Aldermanic nose
  Roll'd its loud diapason after dinner;
And there stood high the holy sconce of Becket,
--Till four assassins came from France to crack it.
