Now we've reached her, lo! the captain,
  Gallant Kid, commands the crew;
Passengers their berths are clapped in,
  Some to grumble, some to spew.
"Hey day! call you that a cabin?
  Why, 'tis hardly three feet square;
Not enough to stow Queen Mab in--
  Who the deuce can harbor there?"
        "Who, sir? plenty--
        Nobles twenty
  Did at once my vessel fill."--
        "Did they? Jesus,
        How you squeeze us!
  Would to God they did so still;
Then I'd 'scape the heat and racket
Of the good ship Lisbon Packet."
