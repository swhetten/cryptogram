She comes from the past and revisits my room;
She looks as she then did, all beauty and bloom;
So smiling and tender, so fresh and so fair,
And yonder she sits in my cane-bottomed chair.
