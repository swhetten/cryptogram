"Those oxen are too good friends to suit me," said a hungry lion.
"They are never far apart, and when I am near them they turn their
tails to one another and show long sharp horns on every side. They
even walk down to the river together when they become thirsty. If
I could catch one of them by himself, I should have a feast."
