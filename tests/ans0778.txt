So the little Gingerbread Boy jumped on
the fox's tail, and the fox swam into the 
river.  When he was a little way from shore
he turned his head, and said, "You are too
heavy on my tail, little Gingerbread Boy,
I fear I shall let you get wet; jump on my
back."
