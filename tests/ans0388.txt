  'And then there's the Butterfly,' Alice went on, after she had
taken a good look at the insect with its head on fire, and had
thought to herself, 'I wonder if that's the reason insects are so
fond of flying into candles--because they want to turn into
Snap-dragon-flies!'
