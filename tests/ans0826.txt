"Then," said the Rabbit, "I will tie this
end of my long rope to you, and I will run
away and tie the other end round my cow,
and when I am ready I will beat my big
drum.  When you hear that, pull very, very
hard, for the cow is stuck very deep in the
mud."
