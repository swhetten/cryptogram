Not so our Nicholas, his meditations
  Still to the same tremendous theme recurred,
The same dread subject of the dark narrations,
  Which, back'd with such authority, he'd heard;
Lost in his own horrific contemplations,
  He pondered o'er each well-remembered word;
When at the bed's foot, close beside the post,
He verily believed he saw--a Ghost!
