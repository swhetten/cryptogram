Fletcher! Murray! Bob! where are you?
  Stretched along the decks like logs--
Bear a hand, you jolly tar, you!
  Here's a rope's end for the dogs.
Hobhouse muttering fearful curses,
  As the hatchway down he rolls,
Now his breakfast, now his verses,
  Vomits forth--and damns our souls.
        "Here's a stanza
        On Braganza--
  Help!"--"A couplet?"--"No, a cup
        Of warm water--"
        "What's the matter?"
  "Zounds! my liver's coming up;
I shall not survive the racket
Of this brutal Lisbon Packet."
