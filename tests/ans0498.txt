  'I wish _I_ could manage to be glad!' the Queen said.  'Only I
never can remember the rule.  You must be very happy, living in
this wood, and being glad whenever you like!'
