A little boy sat at his mother's knees, by
the long western window, looking out into
the garden.  It was autumn, and the wind
was sad; and the golden elm leaves lay
scattered about among the grass, and on
the gravel path.  The mother was knitting
a little stocking; her fingers moved the
bright needles; but her eyes were fixed on
the clear evening sky.
