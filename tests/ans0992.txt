Sometimes from scandalous neglect, my dears, the sleepers sink,
And then you have the carriages upset, as you may think.
The progress of the train, sometimes, a truck or coal-box checks,
And there's a risk for poor Papa's, and every body's necks.
