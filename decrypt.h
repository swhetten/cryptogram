/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _DECRYPT_HEADER_
#define _DECRYPT_HEADER_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "prepare_words.h"
#define ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

char *decrypt(char *input, const match_node_t *headNode);

#endif