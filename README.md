# Cryptogram
Decrypt English messages that were encrypted using a simple substitution cypher.

This program (will someday) take input from stdin that was "encrypted" using a
substitution cypher and will then print to stdout the decrypted (English) version.

This problem was first presented to me as a high school student in 2004 as part
of a University of Utah programming contest. This is a solution written in c.

# Substitution Cypher

A substitution cypher is a simple cypher where each letter of the alphabet is
substituted for another one.

# Use

This solution is designed in 2 parts, the "create the dictionary" bit, and the
"decrypt the string" bit.

## Creating the dictionary

No good decryption of English sentences can work without a long list of English
words! 

In the folder `dictionary` are the tools for creating a dictionary, along with a
word list that I retrieved from [Wiktionary](https://en.wiktionary.org/wiki/Wiktionary:Main_Page).

### Use: Future...

When you `make` in the dictionary directory, it will create the mkdict program.
Use the command below to make the dictionary file for use with the rest of the
solution. The `wordlist file` should contain a list of words 1 per line, which will
populate the final dictionary file. For the best results, wordlist should be in
order of commonality, as the `decrypt the string` bit will try them in the order
they appeared in that file.

> `mkdict [wordlist file] [output file]`

`output file` will be a binary file that is a binary search tree for word lists
based on word "patterns". A "pattern" in this context is the characterization of
a word where the order and position of different characters is captured. E.g.
The word "word" would have the pattern "ABCD", whereas "meme" would be "ABAB",
"four" -> "ABCD", "JAVA" -> "ABCB", etc. This will be used by the `decrypt the string`
bit to find a short list of all the known english words that a given encrypted
word could be based on this characterization that it will use both to help with
decrypting the string, as well as knowing whether it is finished or not.

Note: I intend the "pattern" to (special case) leave "'"s alone. E.g. "don't" ->
"ABC'D" I'm hoping this will make it so that strings that include words with apostrophe's
in them will be a shortcut to victory, as the number of known apostrophe containing
words will be relatively small. Of course, this runs the risk that a sentance
will contain an apostrophe word that is unknown. I'll see in the course of testing
whether this ends up being a boon or a bane.

# Below is the original readme concerning the test suite.

## 2004 Utah High School Programming Contest
## University of Utah

## README


###############################################################################

NEWS
----

+ Mar 09 2004: Second version of the test suite, with new tests 0500--0999.
+ Mar 09 2004: Fixed test case 0212, which contained a non-ASCII character.
+ Mar 03 2004: First version of the test suite, with test cases 0000--0499.


USING THE TEST SUITE
--------------------

The files in this directory are cryptograms and answers that you can use to
test your crypto-analysis program.  The test cases are numbered, and for each
test case there are three files:

  + The `rotXXXX.txt` file contains a cryptogram that is encrypted with a
    rotation cipher.  `XXXX' is the number of the test case, and it is always
    four digits long.

  + The `subXXXX.txt` file contains a cryptogram that is encrypted with a
    substitution cipher.

  + The `ansXXXX.txt` file contains the answer for the test case --- in other
    words, the plain text that your program should output when it deciphers
    either `rotXXXX.txt` or `subXXXX.txt`.

For example, to run your program on the rotation-cipher cryptogram in the first
test case (test case #0000), you would type:

  decrypt rot0000.txt

    --- or, in Java: ---

  java Decrypt rot0000.txt

Your program should output the deciphered message, which is contained in the
`ans0000.txt` file.  The output of your program should EXACTLY MATCH the
contents of the `ans0000.txt` file.  Your program should not output anything
other than the decrypted message.  A good way to test your program is to
capture its output to a file, and then to compare that file to the answer file.

Similarly, to run your program on the substitution-cipher cryptogram in test
case #0000, you would type:

  decrypt sub0000.txt

    --- or, in Java: ---

  java Decrypt sub0000.txt

Again, your program should output the deciphered message contained in the
`ans0000.txt` file.  In summary, `rotXXXX.txt` and `subXXXX.txt` are two
versions of the same message (`ansXXXX.txt`), one encrypted with a rotation
cipher and the other with a substitution cipher.


IMPORTANT NOTES
---------------

The files in this test suite have names that indicate what kind of ciphers were
used to encrypt their contents.  THIS IS FOR *YOUR* BENEFIT ONLY.  Your program
should *NOT* use the name of its input file to decide what kind of cipher it
should be looking for!  When we test the final version of your program, we will
use input files with names that do not indicate the types of ciphers used.

Of course, the answer files are also for *YOUR* benefit only.  Your program
should *NOT* try to read the "answer file" in order to solve a cryptogram.
When we test the final version of your program, it will definitely not have
access to any of the answer files :-).

Some of the cryptograms are going to be very difficult to crack --- the short
cryptograms based on substitution ciphers are likely to be the most difficult.
Your program should be able to do a good job on most of the test cases, but
don't be discouraged if your program cannot crack *all* of the cryptograms.
Just keep improving your program until it can crack as many as possible.

When we evaluate the final version of your program, we will run it on a
combination of files from this test suite (the "practice set") and a set of
new cryptograms (our "test set").  A program that uses general techniques
should work just as well on our test set as it does on this practice set.  A
program that has lots of hacks and tweaks based on this practice set, however,
will probably perform poorly on our test set.

There are too many test cases for you to run them all by hand.  If you want to
test your program with all of the test cases (not required, but also not a bad
idea!), then you will need to write some sort of program or script to automate
the process.  If you do this, be sure to write about what you did in the
`README' file that you submit with your solution to the take-home problem!


THE TEST SUITE
--------------

Test cases 0000--0249 are "Murphy's Law"-style quotes, which are generally
pretty short.  The quotes were taken from the 'murphy' fortune database that is
part of FreeBSD 4.9 <http://www.freebsd.org/>.

Test cases 0250--0499 are paragraphs from _Through_the_Looking_Glass_ by Lewis
Carroll.  These cryptograms are generally longer than the Murphy's Law tests,
and so they can provide more hints to your program!  The electronic version of
_Through_the_Looking_Glass_ was produced by Project Gutenberg
<http://www.gutenberg.net/>.

Test cases 0500--0624 are paragraphs from _Book_of_Etiquette_ by Lillian
Eichler.  These tests are rather long and frequently contain long and/or
unusual words.  The electronic version of this book was produced by Project
Gutenberg.

Test cases 0625--0749 are paragraphs from _Fifty_Famous_Fables_ by Lida Brown
McMurry.  These are parts of children's stories, and so the words are generally
short and simple.  The electronic version of this book was produced by Project
Gutenberg.

Test cases 0750--0874 are paragraphs from _Stories_to_Tell_Children_ by Sara
Cone Bryant.  The language is more complicated than that used in the fables.
The electronic version of this book was produced by Project Gutenberg.

Test cases 0875--0999 are sections from poems contained in _The_Humorous_
_Poetry_of_the_English_Language:_From_Chaucer_to_Saxe_ by James Parton.  The
samples are of varying but mostly "medium" length.  Many of the stanzas have
difficult-to-guess words --- good luck!  The electronic version of this book
was produced by Project Gutenberg.

If we add any new test cases, we will post an announcement on the Web.  Watch
the 2004 Utah High School Programming Contest Web pages for announcements:
<http://www.cs.utah.edu/outreach/contest/2004/index.html>.

###############################################################################

# End of file.
