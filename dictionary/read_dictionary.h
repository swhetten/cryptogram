/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _READ_DICTIONARY_HEADER_
#define _READ_DICTIONARY_HEADER_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "pattern.h"

char **read_dictionary(FILE *fdict, const char *seed, unsigned short int *numWords);

#endif
