/* Copyright (C) 2016 Sean "Ensign" Whetten */
#include "mkdict.h"
#include "make_dictionary.h"
#include "pattern.h"

static int switch_given(int argc, char **argv, const char *wantedSwitch) {
    int i;
    for(i=0;i<argc;++i) 
        if(strcmp(wantedSwitch, argv[i])==0)
            return 1;
    return 0;
}

static int version_or_help_switch_given(int argc, char **argv) {
    return switch_given(argc, argv, "-h") || switch_given(argc, argv, "-v");
}

static int show_usage(const char *programName) {
    printf("%s version %s\nUsage: %s [-h] outputFile\n", 
           programName,
           MKDICT_VERSION_IDENTIFIER,
           programName);
    return 0;
}

int main(int argc, char **argv) {
    int returnCode;
    if(version_or_help_switch_given(argc, argv) || argc != 2)
        returnCode = show_usage(basename(argv[0]));
    else {
        FILE *dict = fopen(argv[1],"w");
        if (!dict) {
            printf("Couldn't open file for writing: %s", argv[1]);
            returnCode = 1;
        } else {
            returnCode = make_and_write_dictionary(stdin, dict);
            fclose(dict);
        }
    }
    fclose(stdout);
    fclose(stderr);
    fclose(stdin);
    return returnCode;
}
