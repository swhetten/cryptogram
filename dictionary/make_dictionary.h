/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _MAKE_DICTIONARY_HEADER_
#define _MAKE_DICTIONARY_HEADER_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "pattern.h"

typedef struct {
    char *pattern;
    int numWords;
	int allocatedWords;
    char **words;
    void *greater;
    void *less;
    void *next;
} pattern_node_t;

int make_and_write_dictionary(FILE *fin, FILE *fout);

#endif
