/* Copyright (C) 2016 Sean "Ensign" Whetten */
#include "matchdict.h"
#include "read_dictionary.h"
#include "pattern.h"

static int print_and_free(char *seed, char **words, unsigned short int numWords) {
    printf("%s", seed);
    int i;
    for(i = 0; i < numWords; ++i) {
        printf(" %s", words[i]);
        free(words[i]);
    }
    free(words);
    printf("\n");
}

static int write_matches(FILE *fdict, FILE *fin) {
    char in[128];
    unsigned short int numWords;
    while (fgets(in, 128, fin)) {
        numWords = 0;
        in[strcspn(in, "\r\n")] = '\0';
        char **words = read_dictionary(fdict, in, &numWords);
        print_and_free(in, words, numWords);
    }
    return 0;
}

static int switch_given(int argc, char **argv, const char *wantedSwitch) {
    int i;
    for(i=0;i<argc;++i) 
        if(strcmp(wantedSwitch, argv[i])==0)
            return 1;
    return 0;
}

static int version_or_help_switch_given(int argc, char **argv) {
    return switch_given(argc, argv, "-h") || switch_given(argc, argv, "-v");
}

static int show_usage(const char *programName) {
    printf("%s version %s\nUsage: %s [-h] dictionaryFile\n", 
           programName,
           MATCHDICT_VERSION_IDENTIFIER,
           programName);
    return 0;
}

int main(int argc, char **argv) {
    int returnCode;
    FILE *dict;
    if(version_or_help_switch_given(argc, argv) || argc != 2)
        returnCode = show_usage(basename(argv[0]));
    else {
        dict = fopen(argv[1],"r");
        if (!dict) {
            printf("Couldn't open dictionary file: %s", argv[1]);
            returnCode = 1;
        } else {
            returnCode = write_matches(dict, stdin);
            fclose(dict);
        }
    }
    fclose(stdout);
    fclose(stderr);
    fclose(stdin);
    return returnCode;
}
