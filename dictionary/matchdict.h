/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _MATCHDICT_HEADER_
#define _MATCHDICT_HEADER_

#include <stdio.h>
#include <libgen.h>
#include <string.h>
#include <stdlib.h>

#define NO_MORE_ARGS -1

#define MATCHDICT_VERSION_IDENTIFIER "0.1"

#endif
