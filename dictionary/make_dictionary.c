#include "make_dictionary.h"
#include "pattern.h"

static void free_all(pattern_node_t *node) {
    pattern_node_t *cur = node;
    pattern_node_t *tmp;
    while(cur != NULL) {
        if(cur->words != NULL) {
            int i;
            for(i = 0; i < cur->numWords; ++i) free(cur->words[i]);
            free(cur->words);
        }
        if(cur->pattern != NULL) free(cur->pattern);
        tmp = cur->next;
        free(cur);
        cur = tmp;
    }
}

static pattern_node_t *new_node(char *word, const char *pattern) {
    pattern_node_t *newNode;
    newNode = (pattern_node_t *)malloc(sizeof(pattern_node_t));
    newNode->pattern = calloc(strlen(pattern) + 1, sizeof(char));
    strcpy(newNode->pattern, pattern);
    newNode->numWords = 1;
    newNode->words = malloc(sizeof(char *));
    newNode->allocatedWords = 1;
    newNode->words[0] = word;
    newNode->greater = NULL;
    newNode->less = NULL;
    newNode->next = NULL;
}

static pattern_node_t *add_word(pattern_node_t *headNode, pattern_node_t *nodeToAddWordTo, char *word) {
    int i;
    for(i = 0; i < nodeToAddWordTo->numWords; ++i)
        if(strcmp(word, nodeToAddWordTo->words[i]) == 0) {
            free(word);
            return headNode;
        }
    if(nodeToAddWordTo->allocatedWords <= nodeToAddWordTo->numWords) {
        nodeToAddWordTo->allocatedWords = nodeToAddWordTo->allocatedWords * 2;
        nodeToAddWordTo->words = realloc(nodeToAddWordTo->words, nodeToAddWordTo->allocatedWords * sizeof(char *));
    }
    nodeToAddWordTo->words[nodeToAddWordTo->numWords] = word;
    nodeToAddWordTo->numWords = nodeToAddWordTo->numWords + 1;
    return headNode;
}

static pattern_node_t *add_new_node(pattern_node_t *headNode, pattern_node_t *nodeToInsertAfter, char *word, const char *pattern) {
    pattern_node_t *newNode = new_node(word, pattern);
    if (nodeToInsertAfter == NULL) {
        newNode->next = headNode;
        return newNode;
    } else {
        newNode->next = nodeToInsertAfter->next;
        nodeToInsertAfter->next = newNode;
        return headNode;
    }
}

static pattern_node_t *find_or_insert_node(pattern_node_t *headNode, char *word, const char *pattern) {
    pattern_node_t *last = NULL;
    pattern_node_t *cur = headNode;
    int cmp;
    while(cur != NULL && (cmp = strcmp(cur->pattern, pattern)) < 0) {
        last = cur;
        cur = cur->next;
    }

    if(cmp == 0)
        return add_word(headNode, cur, word);
    else
        return add_new_node(headNode, last, word, pattern);
}

static pattern_node_t *characterize_with_pattern(pattern_node_t *headNode, char *word) {
    char pattern[strlen(word) + 1];
    get_pattern(word, pattern);
    if(headNode == NULL) 
        return new_node(word, pattern);
    else
        return find_or_insert_node(headNode, word, pattern);
}

static pattern_node_t *characterize(pattern_node_t *headNode, char *word) {
    word = to_uppercase(normalize_word(word));
    char *myWord = calloc(strlen(word) + 1, sizeof(char));
    strcpy(myWord, word);
    return characterize_with_pattern(headNode, myWord);
}

static pattern_node_t *determine_bin_search_head(pattern_node_t *headNode, int count) {
    int pos = count/2;
    if(count%2 == 0) --pos;
    int i;
    pattern_node_t *cur = headNode;
    for(i = 0; i < pos; ++i)
        cur = cur->next;
    if(count % 2 == 1 || cur->numWords >= ((pattern_node_t *)cur->next)->numWords) return cur;
    else return cur->next;
}

static pattern_node_t *determine_bin_search_head_weighted(pattern_node_t *headNode, int count) {
    int pos = count/2;
    if(count%2 == 0) --pos;
    pattern_node_t *cur = headNode;
    pattern_node_t *head = headNode;
    int wordsMax = headNode->numWords;
    int headi = 0;
    int i = 0;
    while(cur = cur->next) {
        if(cur->numWords > wordsMax) {
            wordsMax = cur->numWords;
            head = cur;
            headi = i;
        } else if (cur->numWords == wordsMax && abs(i - pos) < abs(headi - pos)) {
            head = cur;
            headi = i;
        }
        ++i;
    }
    return head;
}

static pattern_node_t *make_binary_search_tree(pattern_node_t *headNode) {
    int count = 0;
    pattern_node_t *cur = headNode;
    while(cur) {
        ++count;
        cur = cur->next;
    }
    if(count < 2)
        return headNode;
    pattern_node_t *binSearchHead = determine_bin_search_head_weighted(headNode, count);
    if(binSearchHead != headNode) {
        pattern_node_t *greaterTail = headNode;
        while(greaterTail->next != binSearchHead) greaterTail = greaterTail->next;
        greaterTail->next = NULL;
        pattern_node_t *greaterBinHead = make_binary_search_tree(headNode);
        binSearchHead->greater = greaterBinHead;
        greaterTail->next = binSearchHead;
    }
    if(binSearchHead->next != NULL) {
        pattern_node_t *lessHead = binSearchHead->next;
        pattern_node_t *lessBinHead = make_binary_search_tree(lessHead);
        binSearchHead->less = lessBinHead;
    }
    return binSearchHead;
}

static int write_string(const char *w, FILE *fout) {
    int i = 0;
    while(w[i]) {
        fputc(w[i], fout);
        ++i;
    }
    fputc('\0', fout);
    return i+1;
}

static int write_int(const unsigned int w, FILE *fout) {
    int i;
    char *posC = (char *)(&w);
    for(i = 0; i < sizeof(unsigned int); i++)
        fputc(posC[i], fout);
    return sizeof(unsigned int);
}

static int write_short_int(const unsigned short int w, FILE *fout) {
    int i;
    char *posC = (char *)(&w);
    for(i = 0; i < sizeof(unsigned short int); i++)
        fputc(posC[i], fout);
    return sizeof(unsigned short int);
}

static int write_bin_tree(pattern_node_t *curNode, FILE *fout, int *numBytesWritten) {
    int myStart = *numBytesWritten;
    if(curNode == NULL) return myStart; 
    *numBytesWritten = *numBytesWritten + write_string(curNode->pattern, fout);
    int posStart = *numBytesWritten;
    *numBytesWritten = *numBytesWritten + write_int((unsigned int)0, fout);
    *numBytesWritten = *numBytesWritten + write_int((unsigned int)0, fout);
    *numBytesWritten = *numBytesWritten + write_short_int((unsigned short int)curNode->numWords, fout);
    int i;
    for(i = 0; i < curNode->numWords; ++i)
        *numBytesWritten = *numBytesWritten + write_string(curNode->words[i], fout);
    int lessPos = 0;
    int greaterPos = 0;
    if(curNode->less != NULL) lessPos = write_bin_tree(curNode->less, fout, numBytesWritten);
    if(curNode->greater != NULL) greaterPos = write_bin_tree(curNode->greater, fout, numBytesWritten);
    fseek(fout, posStart, SEEK_SET);
    write_int((unsigned int)lessPos, fout);
    write_int((unsigned int)greaterPos, fout);
    fseek(fout, 0, SEEK_END);
    return myStart;
}

static void display_linked_list(pattern_node_t *headNode, FILE *fout) {    
    pattern_node_t *cur = headNode;
    while(cur != NULL) {
        fprintf(fout, "%s -- %i : ", cur->pattern, cur->numWords);
        int i;
        for(i = 0; i < cur->numWords; ++i)
            fprintf(fout, "%s ", cur->words[i]);
        fprintf(fout, "\n");
        cur = cur->next;
    }
}

static void display_bin_tree(pattern_node_t *headNode, FILE *fout) {
    fprintf(fout, "%s -", headNode->pattern);
    if(headNode->less != NULL)
        fprintf(fout, "%s", ((pattern_node_t *)headNode->less)->pattern);
    fprintf(fout, " +");
    if(headNode->greater != NULL)
        fprintf(fout, "%s", ((pattern_node_t *)headNode->greater)->pattern);
    fprintf(fout, "\n");
    if(headNode->less != NULL) display_bin_tree(headNode->less, fout);
    if(headNode->greater != NULL) display_bin_tree(headNode->greater, fout);
}

int make_and_write_dictionary(FILE *fin, FILE *fout) {
    pattern_node_t *headNode = NULL;
    char in[128];
    printf("Reading and Characterizing\n");
    int i = 0;
    while(fgets(in, 128, fin)) {
        headNode = characterize(headNode, in);
        if(i > 1000) {
            printf(".");
            fflush(stdout);
            i = 0;
        } else
            ++i;
    }
    printf("\nCalculating Binary Tree\n");
    pattern_node_t *binSearchHead = make_binary_search_tree(headNode);
    
    // display_linked_list(headNode, stdout);
    // fprintf(stdout, "\n");
    //display_bin_tree(binSearchHead, stdout);
    //fprintf(stdout, "\n");
    i = 0;
    printf("Writing Binary Tree to File\n");
    write_bin_tree(binSearchHead, fout, &i);
    
    printf("Freeing Memory\n");
    free_all(headNode);
    return 0;
}