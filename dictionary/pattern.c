/* Copyright (C) 2016 Sean "Ensign" Whetten */
#define ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#include "pattern.h"

static char get_pattern_value(const char c, char *map, int *mapped) {
    if (map[c - ALPHABET[0]] == 0) {
        map[c - ALPHABET[0]] = ALPHABET[*mapped];
        *mapped = *mapped+1;
    }
    return map[c - ALPHABET[0]];
}

char *to_uppercase(char *word) {
    char *w = word;
    while (*w) {
        if ('a' <= *w && 'z' >= *w)
            *w = *w - ('a' - 'A');
        else
            ++w;
    }
    return word;
}

char *normalize_word(char *word) {
    char *w = word;
    while (*w && (*w == ' ' || *w == '\t' || *w == '\n' || *w == '\r'))
        ++w;
    word = w;
    while (*w)
        if (*w == ' ' || *w == '\t' || *w == '\n' || *w == '\r')
            *w = '\0';
        else
            ++w;
    return word;
}

char *get_pattern(const char *word, char *pattern) {
    char mw[strlen(word) + 1];
    strcpy(mw, word);
    char *cur = to_uppercase(normalize_word(mw));
    char *pat = pattern;
    char map[26] = { 0 };
    int mapped = 0;
    while (*cur) {
        *pat = get_pattern_value(*cur, map, &mapped);
        ++cur;
        ++pat;
    }
    *pat = '\0';
    return pattern;
}