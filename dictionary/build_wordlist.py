from string import strip
import re

words = {}
wordmatcher = re.compile('[a-z]+')

def addWord(word, freq):
    word = word.lower()
    match = wordmatcher.match(word)
    if match:
        word = match.group(0)
        if not word in words:
            words[word] = freq
        else:
            words[word] += freq
        
def readdict_wictionary():
    raw = open('procdict_wictionary.txt','r')    
    for line in raw:
        splitl = line.split()
        addWord(splitl[0], float(splitl[1]))
    raw.close()

def addAllTestCases():
    for i in range(999):
        num = str(i).rjust(4, '0')
        raw = open('../tests/ans'+num+'.txt','r')
        data = raw.read()
        raw.close()
        for word in data.split():
            addWord(word, 1)

def addUKACH17():
    raw = open('proc_UKACD17.txt','r')    
    for line in raw:
        addWord(strip(line), 1)
    raw.close()

def addwordlist():
    raw = open('wordlist.txt','r')    
    for line in raw:
        addWord(strip(line), 1)
    raw.close()

def getval(word):
    return words[word]
    
def output():
    s = sorted(words, key=getval, reverse=True)
    for word in s:
        print "{0}".format(word)

if __name__ == "__main__": 
	readdict_wictionary();
	addAllTestCases();
	addUKACH17();
	addwordlist();
	output();


