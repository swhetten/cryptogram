/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _MKDICT_HEADER_
#define _MKDICT_HEADER_

#include <stdio.h>
#include <libgen.h>
#include <string.h>
#include <stdlib.h>

#define NO_MORE_ARGS -1

#define MKDICT_VERSION_IDENTIFIER "0.1"

#endif
