#include "read_dictionary.h"
#include "pattern.h"

static char *read_string(FILE *f, const int maxRead, char *buffer) {
    char c;
    int i = 0;
    while((c = fgetc(f)) && i < maxRead) {
        buffer[i] = c;
        ++i;
    }
    buffer[i] = c;
    return buffer;
}

static unsigned int read_int(FILE *f) {
    int i;
    unsigned int out = 0;
    char *posC = (char *)(&out);
    for(i = 0; i < sizeof(unsigned int); i++)
        posC[i] = fgetc(f);
    return out;
}

static unsigned short int read_short_int(FILE *f) {
    int i;
    unsigned short int out = 0;
    char *posC = (char *)(&out);
    for(i = 0; i < sizeof(unsigned short int); i++)
        posC[i] = fgetc(f);
    return out;
}

static char **read_words(const char *seed, FILE *fdict, unsigned short int *numWords) {
    *numWords = read_short_int(fdict);
    char **words = calloc(*numWords, sizeof(char *));
    int i;
    char in[128];
    for(i = 0; i < *numWords; ++i) {
        read_string(fdict, 128, in);
        char *word = calloc(strlen(in) + 1, sizeof(char));
        strcpy(word, in);
        words[i] = word;
    }
    return words;
}

static char **find_words(const char *seed, const char *pattern, FILE *fdict, unsigned short int *numWords) {
    fseek(fdict, 0, SEEK_SET);
    char in[128];
    read_string(fdict, 128, in);
    int cmp;
    do {
        cmp = strcmp(in, pattern);
        unsigned int next = read_int(fdict);
        if(cmp >= 0)
            next = read_int(fdict);
        if(cmp == 0) 
            return read_words(seed, fdict, numWords);
        if(next == 0) return NULL;
        fseek(fdict, next, SEEK_SET);
        read_string(fdict, 128, in);
    } while (cmp != 0 && !feof(fdict));
    return NULL;
}

char **read_dictionary(FILE *fdict, const char *seed, unsigned short int *numWords) {
    char in[strlen(seed) + 1];
    strcpy(in, seed);
    char *normalized = normalize_word(in);
    char pattern[strlen(normalized) + 1];
    get_pattern(normalized, pattern);
    return find_words(normalized, pattern, fdict, numWords);
}
