/* Copyright (C) 2016 Sean "Ensign" Whetten */
#ifndef _PATTERN_HEADER_
#define _PATTERN_HEADER_

#include <string.h>

char *normalize_word(char *word);
char *to_uppercase(char *word);
char *get_pattern(const char *word, char *pattern);

#endif
