/* Copyright (C) 2016 Sean "Ensign" Whetten */
#include "cryptogram.h"

static int print_stuff(char *input, match_node_t *words) {
    match_node_t *cur = words;
    match_node_t *last = NULL;
    while(cur) {
        printf("%s - %i - %i\n", cur->seed, cur->numWords, cur->occurances);
        last = cur;
        cur = cur->next;
    }
    // while(last) {
        // printf("%s - %i - %i\n", last->seed, last->numWords, last->occurances);
        // last = last->last;
    // }
}

static int read_in_and_decrypt(FILE *fdict, FILE *fin) {
    char buffer[256];
    char *input = NULL;
    char *cur = NULL;
    size_t totalLen = 0;
    size_t len = 0;
    while((len = fread(buffer, sizeof(char), 256, fin)) == 256) {
        totalLen = totalLen + len;
        input = realloc(input, totalLen);
        cur = input + totalLen - len;
        memcpy(cur, buffer, len);
    }
    totalLen = totalLen + len + 1;
    input = realloc(input, totalLen);
    cur = input + totalLen - len - 1;
    memcpy(cur, buffer, len);
    input[totalLen-1] = '\0';
    
    match_node_t *words = prepare_words(fdict, input, totalLen);
    printf("%s\n\n", input);   
    decrypt(input, words);
    //print_stuff(input, words);
    printf("%s\n\n", input);    
    free_all_match_nodes(words);
    
    return 0;
}

static int switch_given(int argc, char **argv, const char *wantedSwitch) {
    int i;
    for(i=0;i<argc;++i) 
        if(strcmp(wantedSwitch, argv[i])==0)
            return 1;
    return 0;
}

static int version_or_help_switch_given(int argc, char **argv) {
    return switch_given(argc, argv, "-h") || switch_given(argc, argv, "-v");
}

static int show_usage(const char *programName) {
    printf("%s version %s\nUsage: %s [-h] dictionaryFile\n", 
           programName,
           CRYPTOGRAM_VERSION_IDENTIFIER,
           programName);
    return 0;
}

int main(int argc, char **argv) {
    int returnCode;
    FILE *dict;
    if(version_or_help_switch_given(argc, argv) || argc != 2)
        returnCode = show_usage(basename(argv[0]));
    else {
        dict = fopen(argv[1],"r");
        if (!dict) {
            printf("Couldn't open dictionary file: %s", argv[1]);
            returnCode = 1;
        } else {
            returnCode = read_in_and_decrypt(dict, stdin);
            fclose(dict);
        }
    }
    fclose(stdout);
    fclose(stderr);
    fclose(stdin);
    return returnCode;
}
