all: cryptogram
	printf "words@words.com" | ./cryptogram crydict
	printf "this some.this" | ./cryptogram crydict
	cat tests/ans0000.txt | ./cryptogram crydict
	cat tests/ans0666.txt | ./cryptogram crydict
	cat tests/ans0777.txt | ./cryptogram crydict

cryptogram: cryptogram_main.c cryptogram.h dictionary/pattern.c dictionary/pattern.h dictionary/read_dictionary.c dictionary/read_dictionary.h prepare_words.c prepare_words.h decrypt.c decrypt.h
	gcc -o cryptogram cryptogram_main.c prepare_words.c dictionary/pattern.c dictionary/read_dictionary.c decrypt.c

clean:
	rm *.exe
